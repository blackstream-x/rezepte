# Irish Scones

_vegetarisch_

!!! note "Quelle"

    Variation eines Rezepts von chefkoch.de:
    <https://www.chefkoch.de/rezepte/670481169114179/Original-Irish-Scones.html>

---

## Zutaten für 14–16 Scones

* 340 g Mehl
* 2 TL Backpulver
* 110 g Zucker
* 110 g Butter bzw. Margarine 
* 140 ml (1/4 Pint) Milch oder z.B. Hafermilch 
* 1 Ei
* eine Prise Salz

optional:

* 150 g Rosinen

## Zubereitung

Zunächst die trockenenen Zutaten vermengen,
dann mit Butter oder Margarine, Milch und Ei zu einem glatten Teig verarbeiten.
Falls verwendet, Rosinen unterheben

Mit 2 Esslöffeln kleine Klumpen formen und auf einem mit Backpapier ausgelegten Backblech verteilen.

Bei 180°C (Umluft ~ 160°C) ca. 20 Minuten backen.

