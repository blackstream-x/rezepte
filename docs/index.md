# Übersicht

Kleine Rezeptsammlung, hauptsächlich vegetarisch.

## Gebäck

* [Irish Scones](./gebäck/irish_scones.md)

## Mexico

_mexikanisch angehauchte Gerichte_

* [Chili sin carne](mexico/chili_sin_carne.md)
* [Huevos Rancheros](mexico/huevos_rancheros.md)

